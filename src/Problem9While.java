import java.util.Scanner;

public class Problem9While {
    public static void main(String[] args) {
        int num;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input n: ");
        num = sc.nextInt();
        int i = 0;
        while (i <= num){
            int j = 1;
            while (j <= num){
                System.out.print(j);
                j++;
            }
            System.out.println();
            i++;
        }
    }
}
