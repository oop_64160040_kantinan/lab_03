import java.util.Scanner;

public class Problem5 {
    public static void main(String[] args) {
        String str;
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Please input: ");
            str = sc.nextLine();
            System.out.println(str);
            if (str.equalsIgnoreCase("bye")) {
                System.out.println("Exit Program");
                System.exit(0);
            }
        }

    }
}
